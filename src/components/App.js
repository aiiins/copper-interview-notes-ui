import React, { Component } from "react";
import Form from "./Form";

export default function App(props) {
  return (
    <div className="columns is-mobile is-multiline">
      <h1 className="has-text-centered column title">A R I A</h1>
      <article className="content column is-full">
        Welcome to Aptitive's Recruiting Interview Applicaiton. <br />
        <br />I assume you're here because you've just interviewed someone.
        Please fill out the form below with whatever you gleaned from your
        conversation with the candidate.
      </article>
      <Form data={props.data} />
    </div>
  );
}
