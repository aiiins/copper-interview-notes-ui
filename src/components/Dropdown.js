import React, { Component } from "react";

export default class Dropdown extends Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    e.preventDefault();
    this.props.handleInput(e);
  }

  render() {
    return (
      <div
        className="field"
        id={this.props.id + "-dropdown"}
        key={this.props.id}
      >
        <label htmlFor={this.props.name} key={this.props.id} className="label">
          {this.props.label}
        </label>
        <select
          name={this.props.name}
          onChange={this.handleChange}
          value={this.props.selected}
          className="select is-size-6"
        >
          {this.props.values.map(o => (
            <option key={Math.random()} value={o.name}>
              {o.name}
            </option>
          ))}
        </select>
      </div>
    );
  }
}
