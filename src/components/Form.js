import React, { Component } from "react";
import Dropdown from "./Dropdown";
import Input from "./Input";
import { FORM_FIELDS } from "../CONSTANTS";

export default class Form extends Component {
  constructor(props) {
    super(props);

    this.state = {
      candidate: "",
      interviewType: ""
    };

    this.handleInput = this.handleInput.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInput(e) {
    const target = e.target;
    const { name, value } = target;

    this.setState({ [name]: value });
  }

  handleSubmit(e) {
    e.preventDefault();
  }

  render() {
    const data = this.props.data;
    const preSelection = Object.keys(data).filter(
      k => data[k].formPart === "preSelection"
    );

    const interviewType = this.state.interviewType.toUpperCase();
    const formFields = {
      //TODO: how to order these ...
      ...FORM_FIELDS.COMMON,
      ...FORM_FIELDS[interviewType]
    };

    return (
      <form
        id="interview-notes-form"
        className="column is-full"
        onSubmit={this.handleSubmit}
      >
        <div id="pre-selection">
          {preSelection.map(k => (
            <Dropdown
              name={data[k].name}
              values={["", ...data[k].values]}
              key={k}
              id={k}
              handleInput={this.handleInput}
              label={data[k].label}
              selected={this.state[k]}
            />
          ))}
        </div>
        <hr />
        <div id="notes-form">
          {this.state.candidate &&
            interviewType &&
            Object.keys(formFields)
              .sort(
                (cur, nxt) =>
                  formFields[cur].position || 0 - formFields[nxt].position || 0
              )
              .map(k => (
                <Input
                  id={k}
                  key={k}
                  name={k}
                  type={formFields[k].inputType}
                  displayName={formFields[k].displayName}
                  handleInput={this.handleInput}
                  interviewType={interviewType}
                  selected={this.state[k]}
                />
              ))}
        </div>
        {this.state.candidate && interviewType && (
          <div className="column has-text-centered">
            <hr />
            <input className="button" type="submit" id="submit" />
          </div>
        )}
      </form>
    );
  }
}
