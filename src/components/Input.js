import React, { Component, Fragment } from "react";

export default function Input(props) {
  return (
    <div className="field" id={props.id + "-input-container"} key={props.id}>
      {props.type === "yesno" ? (
        <YesNo
          labels={["Yes", "No"]}
          name={props.name}
          onChange={e => props.handleInput(e)}
          displayName={props.displayName}
          selected={props.selected}
        />
      ) : props.type === "scale" ? (
        <NumericScale
          labels={[1, 2, 3, 4, 5]}
          name={props.name}
          onChange={e => props.handleInput(e)}
          displayName={props.displayName}
          selected={props.selected}
        />
      ) : props.type === "textarea" ? (
        <div className="control">
          <label className="label" htmlFor={props.name}>
            {props.displayName}
          </label>
          <textarea
            name={props.name}
            className="textarea"
            rows={4}
            cols={90}
            onChange={e => props.handleInput(e)}
          />
        </div>
      ) : props.type === "multiselect" ? (
        <MultiSelect
          options={[
            "Flexible / Versatile",
            "Collaborative",
            "Transparent",
            "Challenging (A debater / skeptic)",
            "A dreamer (A creative type)",
            "Executive (A doer)",
            "Brainy (A thinker)"
          ]}
          displayName={props.displayName}
          name={props.name}
          // onChange={e => props.handleInput(e)}
        />
      ) : (
        <div className="control">
          <label className="label" htmlFor={props.name}>
            {props.displayName}
          </label>
          <input
            type={props.type}
            name={props.name}
            className="input"
            onChange={e => props.handleInput(e)}
          />
        </div>
      )}
    </div>
  );
}

function YesNo(props) {
  return (
    <div className="control">
      <label className="label" htmlFor={props.name}>
        {props.displayName}
      </label>
      <div className={`column buttons has-addons control`}>
        {props.labels.map(label => (
          <button
            onClick={e => props.onChange(e)}
            className={`button ${
              props.selected === label.toLowerCase() ? "is-dark" : ""
            }`}
            name={props.name}
            key={label}
            value={label.toLowerCase()}
          >
            {label}
          </button>
        ))}
      </div>
    </div>
  );
}

function NumericScale(props) {
  return (
    <div className="control">
      <label className="label" htmlFor={props.name}>
        {props.displayName}
      </label>
      <div className={`column buttons has-addons control`}>
        {props.labels.map(label => (
          <button
            onClick={e => props.onChange(e)}
            className={`button ${
              parseInt(props.selected) === props.labels.indexOf(label) + 1
                ? "is-dark"
                : ""
            }`}
            name={props.name}
            key={label}
            value={props.labels.indexOf(label) + 1}
          >
            {label}
          </button>
        ))}
      </div>
    </div>
  );
}

class MultiSelect extends React.Component {
  constructor(props) {
    super(props);
    this.MAX_SELECTED = 3;

    this.state = {
      selected: []
    };

    this.handleMultiSelect = this.handleMultiSelect.bind(this);
    // this.handleClick = this.handleClick.bind(this);
  }

  handleMultiSelect(e) {
    e.persist();

    const { value } = e.target;
    console.log(value);

    this.setState(state => {
      if (state.selected.indexOf(value) !== -1) {
        return { selected: state.selected.filter(v => v !== value) };
      }
      return { selected: [...state.selected, value] };
    });
  } // }

  render() {
    return (
      <div className="control">
        <label className="label" htmlFor={this.props.name}>
          {this.props.displayName}
        </label>
        <div className="select is-multiple">
          <select
            multiple
            size={this.props.options.length}
            name={this.props.name}
            onChange={e => this.handleMultiSelect(e)}
            value={this.state.selected}
          >
            {this.props.options.map(option => (
              <option value={option.toLowerCase()}>{option}</option>
            ))}
          </select>
        </div>
      </div>
    );
  }
}
