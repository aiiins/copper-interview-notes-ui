import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";
import "./styles.scss";

(() => {
  // should definitely be hardcoded forever
  const url =
    "https://copperwebhooks.azurewebsites.net/api/CopperOpportunities?code=CfDJ8AAAAAAAAAAAAAAAAAAAAACe55iaE9rOPWXLaEECW1j_h6PZx0zGMxplv-u07oFaKVbR-1LWfCCDeuj8ycqRVXVKopb33xNIBvI1aQUmiK3CwM3ZFCeuzaJ4pLnlA1lwvrsx6D1t5aYCo4wsqpnrgLENVlsQEydyn_THs0At5rBnekhFHpMHQr3FCsgKD0EHwg";
  const options = {
    method: "POST",
    "Access-Control-Allow-Origin": "*"
  };

  const RECRUITING_PIPELINE_CODE = 513250;

  const root = document.getElementById("root");

  fetch(url, options)
    .then(res =>
      res.ok ? res.json() : new Error("Error fetching resource\n" + res)
    )
    .then(json => {
      return json;
    })
    // rearrange json for frontend consumption
    .then(json => ({
      candidate: {
        // only get the "Candidates" pipeline
        label: "Candidate: ",
        values: json["opportunities"].filter(
          o => o["pipeline_id"] == RECRUITING_PIPELINE_CODE
        ),
        formPart: "preSelection",
        name: "candidate"
      },
      interviewType: {
        label: "Interview Type: ",
        values: [
          { name: "Prescreen" },
          { name: "Technical" },
          { name: "Management" }
        ],
        formPart: "preSelection",
        name: "interviewType"
      }
    }))
    .then(data => ReactDOM.render(<App data={data} />, root))
    .catch(err => console.log(err));
})();
