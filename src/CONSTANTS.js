// three kinds of interviews + some common fields
export const FORM_FIELDS = {
  COMMON: {
    cultureFit: {
      displayName: "Culture Fit",
      inputType: "scale"
    },
    notes: {
      displayName: "General Notes",
      inputType: "textarea",
      position: 99
    },
    shouldAdvance: {
      displayName: "Advance to Next Step",
      inputType: "yesno",
      position: 100
    },
    interviewerName: {
      displayName: "Interviewer Name",
      inputType: "text",
      position: -1
    },
    topThreeDescriptors: {
      displayName: "3 Ways to Describe the Candidate",
      inputType: "multiselect",
      position: 98
    }
  },

  PRESCREEN: {
    desiredSalary: {
      displayName: "Desired Salary",
      inputType: "text"
    },
    wouldCommute: {
      displayName: "Would Commute",
      inputType: "yesno"
    },
    abilityTolearn: {
      displayName: "Ability to Learn",
      inputType: "scale"
    }
  },

  TECHNICAL: {
    abilityTolearn: {
      displayName: "Ability to Learn",
      inputType: "scale"
    },
    technicalAptitide: {
      displayName: "Tech. Aptitude",
      inputType: "scale"
    },
    problemSolving: {
      displayName: "Problem Solving Ability",
      inputType: "scale"
    },
    cloudExp: {
      displayName: "Cloud Experience",
      inputType: "scale"
    }, // experience
    bizIntelExp: {
      displayName: "BI Experience",
      inputType: "scale"
    },
    strategyExp: {
      displayName: "Strategy Experience",
      inputType: "scale"
    },
    appDevExp: {
      displayName: "App Dev Experience",
      inputType: "scale"
    },
    techLimits: {
      displayName: "Tech. Limitations",
      inputType: "text"
    }
  },

  MANAGEMENT: {
    technicalAptitide: {
      displayName: "Tech. Aptitude",
      inputType: "scale"
    },
    leadership: {
      displayName: "Leadership",
      inputType: "scale"
    },
    communication: {
      displayName: "Communication/Presence",
      inputType: "scale"
    },
    passion: {
      displayName: "Passion/Drive",
      inputType: "scale"
    },
    teamPlayer: {
      displayName: "Team Player",
      inputType: "scale"
    },
    isSecond: {
      displayName: "Second Mgmt. Interview",
      inputType: "yesno"
    }
  }
};
