# ARIA UI

Here's the stuff you need to know!

### Get Started

```bash
nvm use # might not need
npm i
npm run start
```

### Deploy

[Azure Static Sites](https://aptinterviewstatic.z19.web.core.windows.net/)  
Use the [Azure tools for VSCode](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-azurestorage).

### Style

[Bulma](https://bulma.io/) & [Sass](https://sass-lang.com/)

### Build

[Parcel](https://parceljs.org/) &
[nvm](https://github.com/creationix/nvm)

Just run `npm run build`.

### Issues

Just a few lil thangs :)

#### Minor

- field validation (ie salary)

#### Major

- `<MultiSelect />` is borked :(
